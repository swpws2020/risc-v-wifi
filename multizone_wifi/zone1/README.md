# RISC-V Multizone experiment

## Getting started

To follow this guide, you need to have [Freedom Studio](https://github.com/sifive/freedom-studio) installed.

### Importing the project

First clone this repository and import the `sifive_software` folder into Freedom Studio.
You can do that by clicking on "File -> Import -> General -> Existing Projects into Workspace -> Next -> Browse", selecting the the cloned `sifive_software` folder then clicking on "Finish".

### Setting up WiFi connection parameters

Next you need to add your WiFi credentials in order to connect to your network.
To do this, create the file `sifive_software/src/wifi_data.h` with the following content, replacing `<SSID>` with the SSID of your WiFi network and `<PWD>` with the password for your WiFi network:

```c
#ifndef WIFI_DATA_H_
#define WIFI_DATA_H_

#define WIFI_SSID "<SSID>"
#define WIFI_PASSWORD "<PWD>"

#endif
```

### Running the project on the HiFive1 Rev B

First you need to build the project, by clicking on "Project -> Build All".
The resulting files should be located in the `sifive_software/src/debug` folder.

The easiest method of actually running the project is through the Freedom Studio debugger.
To configure the debugger, click on "Run -> Debug Configurations..." and then right click on "SiFive GDB SEGGER J-Link Debugging" to create a "New Configuration".

Make sure that in the text field "C/C++ Application" in the tab "Main" contains the value "src/debug/wifi_exploit.elf".
Next you need to click on the "Target" tab and select the "bsp/design.dts" file for the "DTS File" field.

Now you should be able to debug the project through Freedom Studio (for example by clicking on "Run -> Debug").

Please note that we have had problems with the actual debugging.
While this process does successfully flash the latest version of the code onto the board, **you may need to press the reset button on the board in order for it to correctly connect with the contained ESP32 chip and therefore the WiFi**, disconnecting the debugger.
