/*
 * esp32.h
 *
 *  Created on: Nov 28, 2020
 *      Author: niclas
 */

#ifndef ESP32_H_
#define ESP32_H_

#include <stdint.h>
#include <stddef.h>

enum esp32_result {
	ESP32_OK,
	ESP32_ERR,
};

enum esp32_event {
	ESP32_NEW_CONNECTION,
	ESP32_CLOSED_CONNECTION,
	ESP32_MSG_RECEIVED,
	ESP32_UNKNOWN_EVENT
};

// Initializes the ESP32 chip.
// All other methods assume that this was initially called.
enum esp32_result esp32_init(void);
// Connects the ESP32 to the access point with the given SSID and password.
enum esp32_result esp32_connect_to_ap(const char *ssid, const char *pwd);
// Disconnects the ESP32 from an access point.
enum esp32_result esp32_disconnect_from_ap(void);
// Opens a server on the ESP32 with the given port.
enum esp32_result esp32_open_server(uint16_t port);
// Closes a server on the ESP32 with the given port.
enum esp32_result esp32_close_server(void);
// Waits for one of the event types, returning the associated data.
// The caller is responsible for calling free on the returned data pointer.
enum esp32_event esp32_wait_for_event(uint32_t *id, uint32_t *len, char **data);
// Sends a packet to the connection with the given id.
enum esp32_result esp32_send(uint32_t id, uint32_t len, char *data);
// Closes the connection with the given id.
enum esp32_result esp32_close_connection(uint32_t id);

#endif /* ESP32_H_ */
