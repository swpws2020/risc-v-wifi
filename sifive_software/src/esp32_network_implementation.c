#include "esp32_network_implementation.h"
#include "wifi_data.h"
#include "esp32.h"

int esp32_network_implementation_init(void *custom_data) {
	if(esp32_init() != ESP32_OK) {
	    printf("Could not initialize ESP32.\r\n");
	    return 1;
	}

	if(esp32_connect_to_ap(WIFI_SSID, WIFI_PASSWORD) != ESP32_OK) {
		printf("Could not connect to the access point.\r\n");
		return 2;
	}

	if(esp32_open_server(80) != ESP32_OK) {
		printf("Could not open server.\r\n");
		esp32_disconnect_from_ap();
		return 3;
	}

	return 0;
}

enum network_event esp32_network_implementation_recv_data_or_connection(void *custom_data, int *connection_id, uint32_t *len, char **data) {
	// Loop until one of the event types supported by the server occurs.
	while(1) {
		enum esp32_event event_type = esp32_wait_for_event((uint32_t *)connection_id, len, data);

		switch(event_type) {
		case ESP32_NEW_CONNECTION:
			return NEW_CONNECTION;
		case ESP32_CLOSED_CONNECTION:
			return CLOSED_CONNECTION;
		case ESP32_MSG_RECEIVED:
			return DATA_RECEIVED;
		case ESP32_UNKNOWN_EVENT:
			break;
		}
	}
}

int esp32_network_implementation_close_connection(void *custom_data, int connection_id) {
	if(esp32_close_connection(connection_id) != ESP32_OK) {
		return 1;
	}
	return 0;
}

int esp32_network_implementation_send_data(void *custom_data, int connection_id, uint32_t len, char *data) {
	if(esp32_send(connection_id, len, data) != ESP32_OK) {
		return 1;
	}
	return 0;
}

struct network_implementation ESP32_NETWORK_IMPLEMENTATION = {
	.init = esp32_network_implementation_init,
	.recv_data_or_connection = esp32_network_implementation_recv_data_or_connection,
	.close_connection = esp32_network_implementation_close_connection,
	.send_data = esp32_network_implementation_send_data,
	.custom_data = NULL,
};
