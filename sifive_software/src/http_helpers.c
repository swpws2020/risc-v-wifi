#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "http_helpers.h"

char *http_status_code_str(uint16_t status_code) {
	switch(status_code) {
	case 100: return "Continue";
	case 101: return "Switching Protocols";
	case 102: return "Processing";
	case 200: return "OK";
	case 201: return "Created";
	case 202: return "Accepted";
	case 203: return "Non-authoritative Information";
	case 204: return "No Content";
	case 205: return "Reset Content";
	case 206: return "Partial Content";
	case 207: return "Multi-Status";
	case 208: return "Already Reported";
	case 226: return "IM Used";
	case 300: return "Multiple Choices";
	case 301: return "Moved Permanently";
	case 302: return "Found";
	case 303: return "See Other";
	case 304: return "Not Modified";
	case 305: return "Use Proxy";
	case 307: return "Temporary Redirect";
	case 308: return "Permanent Redirect";
	case 400: return "Bad Request";
	case 401: return "Unauthorized";
	case 402: return "Payment Required";
	case 403: return "Forbidden";
	case 404: return "Not Found";
	case 405: return "Method Not Allowed";
	case 406: return "Not Acceptable";
	case 407: return "Proxy Authentication Required";
	case 408: return "Request Timeout";
	case 409: return "Conflict";
	case 410: return "Gone";
	case 411: return "Length Required";
	case 412: return "Precondition Failed";
	case 413: return "Payload Too Large";
	case 414: return "Request-URI Too Long";
	case 415: return "Unsupported Media Type";
	case 416: return "Requested Range Not Satisfiable";
	case 417: return "Expectation Failed";
	case 418: return "I'm a teapot";
	case 421: return "Misdirected Request";
	case 422: return "Unprocessable Entity";
	case 423: return "Locked";
	case 424: return "Failed Dependency";
	case 426: return "Upgrade Required";
	case 428: return "Precondition Required";
	case 429: return "Too Many Requests";
	case 431: return "Request Header Fields Too Large";
	case 444: return "Connection Closed Without Response";
	case 451: return "Unavailable For Legal Reasons";
	case 499: return "Client Closed Request";
	case 500: return "Internal Server Error";
	case 501: return "Not Implemented";
	case 502: return "Bad Gateway";
	case 503: return "Service Unavailable";
	case 504: return "Gateway Timeout";
	case 505: return "HTTP Version Not Supported";
	case 506: return "Variant Also Negotiates";
	case 507: return "Insufficient Storage";
	case 508: return "Loop Detected";
	case 510: return "Not Extended";
	case 511: return "Network Authentication Required";
	case 599: return "Network Connect Timeout Error";
	default: return NULL;
	}
}

char *http_prepare_response(char *content, uint32_t content_len, uint16_t status_code, uint32_t *out_len) {
	char *status_code_str = http_status_code_str(status_code);

	if(status_code_str == NULL) {
		content = "Internal server error.";
		content_len = strlen(content);
		status_code = 500;
		status_code_str = http_status_code_str(status_code);
	}

	uint32_t status_code_len = strlen(status_code_str);
	// 70 bytes for the protocol version, response code, two headers and newlines should be enough.
	uint32_t buf_len = content_len + status_code_len + 70;
	char *buf = malloc(buf_len);

	int written_len = snprintf(
		buf,
		buf_len,
		"HTTP/1.1 %u %s\r\n"
		"Content-Length: %u\r\n"
		"Connection: Close\r\n"
		"\r\n",
		status_code,
		status_code_str,
		content_len
	);

	if(written_len + content_len >= buf_len) {
		printf("buffer for server reply is not big enough\r\n");
        free(buf);
		return NULL;
	}

	memcpy(buf + written_len, content, content_len);

	*out_len = written_len + content_len;

	return buf;
}
