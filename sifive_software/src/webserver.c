#include "webserver.h"

static struct network_implementation NETWORK;

int webserver_init(struct network_implementation network) {
	NETWORK = network;
	return NETWORK.init(NETWORK.custom_data);
}

enum network_event webserver_recv_data_or_connection(int *connection_id, uint32_t *len, char **data) {
	return NETWORK.recv_data_or_connection(NETWORK.custom_data, connection_id, len, data);
}

int webserver_close_connection(int connection_id) {
	return NETWORK.close_connection(NETWORK.custom_data, connection_id);
}

int webserver_send_data(int connection_id, uint32_t len, char *data) {
	return NETWORK.send_data(NETWORK.custom_data, connection_id, len, data);
}

void webserver_handle_request(request_handler handler, int id, uint32_t len, char *data) {
	char *end_ptr = &data[len];
	char *current_position = data;

	enum request_type type;
	if(len > 4 && strncmp(data, "GET ", 4) == 0) {
		// We have what seems like a GET request.
		type = GET_REQUEST;
		current_position = &data[4];
	} else if(len > 5 && strncmp(data, "POST ", 5) == 0) {
		// We have what seems like a POST request.
		type = POST_REQUEST;
		current_position = &data[5];
	} else {
		// We have an unknown or invalid request. Just drop it.
		return;
	}

	char *location = current_position;

	while(current_position < end_ptr && *current_position != ' ') {
		++current_position;
	}

	if(current_position == end_ptr) {
		// We have an invalid request, drop it.
		return;
	}

	// Zero terminate the location for the request handler.
	*current_position = '\0';
	++current_position;

	// Go to the end of the first line.
	while(current_position < end_ptr && strncmp(current_position - 1, "\r\n", 2) != 0) {
		++current_position;
	}

	if(current_position == end_ptr) {
		// We have an invalid request, drop it.
		return;
	}
	++current_position;

	uint32_t length = 0;

	// Parse all headers.
	while(current_position < end_ptr && strncmp(current_position - 4, "\r\n\r\n", 4) != 0) {
		char *header_start = current_position;
		while(current_position < end_ptr && strncmp(current_position - 1, "\r\n", 2) != 0) {
			++current_position;
		}

		if(header_start + 16 < end_ptr && strncmp(header_start, "Content-Length: ", 16) == 0) {
			length = atoi(header_start + 16);
		}
		++current_position;
	}

	if(strncmp(current_position - 4, "\r\n\r\n", 4) != 0) {
		// We have an invalid request, drop it.
		return;
	}

	// Now that we're done parsing, call the handler and send the response.
	if(handler != NULL) {
		bool free_response;
		uint32_t response_len;
		char *response = handler(location, type, current_position, length, &response_len, &free_response);
		if(response != NULL) {
			if(webserver_send_data(id, response_len, response) != 0) {
				// Just ignore errors for now since we have no better way of handling them.
				printf("These was an error sending the response: %s\r\n", response);
			}
			if(free_response) {
				free(response);
			}
		}
	}
}

void webserver_run(request_handler handler) {
	// We assume here for now that each request fully arrives in one packet.
	// Because of the limited amount of memory, it would not generally be possible to save intermediate packets
	// and for our use case this should suffice
	while(1) {
		int id;
		uint32_t len;
		char *data;
		switch(webserver_recv_data_or_connection(&id, &len, &data)) {
		case NEW_CONNECTION:
			// Just accept the connection and wait for data to arrive.
			// A denial of service attack is not really anything we need to worry about for this project.
			printf("(%d) The web server accepted a new connection.\r\n", id);
			break;
		case CLOSED_CONNECTION:
			// Since we just accept connections without specially handling them, there is nothing to do here.
			printf("(%d) Connection closed.\r\n", id);
			break;
		case DATA_RECEIVED:
			webserver_handle_request(handler, id, len, data);
			webserver_close_connection(id);
			break;
		case ERROR:
			// Close the connection on errors, as there is likely little we can do to fix them.
			webserver_close_connection(id);
			printf("(%d) An error occurred while waiting for data to arrive.\r\n", id);

			break;
		}
		free(data);
	}
}

uint32_t webserver_percent_decode(char *data, uint32_t len) {
	// Decode in-place according to https://url.spec.whatwg.org/#percent-decode and https://url.spec.whatwg.org/#urlencoded-parsing
	// The in-place decoding works, because in_index can only be "faster" than out_index, not "slower".
	// The new length is returned.
	// If The length is reduced, a '\0'-byte is appended at the end, but not included with the length.

	uint32_t in_index = 0, out_index = 0;

	while(in_index < len) {
		if(data[in_index] == '+') {
			data[out_index] = ' ';
		} else if(data[in_index] == '%' && in_index + 2 < len) {
			char bytes[2];
			for(int i = 0; i < 2; ++i) {
				if('0' <= data[in_index + i + 1] && data[in_index + i + 1] <= '9') {
					bytes[i] = data[in_index + i + 1] - '0';
				} else if('a' <= data[in_index + i + 1] && data[in_index + i + 1] <= 'f') {
					bytes[i] = data[in_index + i + 1] - 'a' + 10;
				} else if('A' <= data[in_index + i + 1] && data[in_index + i + 1] <= 'F') {
					bytes[i] = data[in_index + i + 1] - 'A' + 10;
				} else {
					bytes[i] = 16;
				}
			}

			if(bytes[0] == 16 || bytes[1] == 16) {
				data[out_index] = data[in_index];
			} else {
				data[out_index] = (bytes[0] << 4) | bytes[1];
				in_index += 2;
			}
		} else {
			data[out_index] = data[in_index];
		}

		++in_index;
		++out_index;
	}

	if(out_index < in_index) {
		data[out_index] = '\0';
	}

	return out_index;
}
