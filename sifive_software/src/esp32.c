// The ESP32 is connected via a SPI interface
// The communication works by sending it commands (for details see spi.c) and receiving answers
// This is done with a text based protocol (found at https://www.espressif.com/sites/default/files/documentation/esp32_at_instruction_set_and_examples_en.pdf)
// This file aims to build an abstraction on top of that protocol, resulting in a more high-level API
// for accessing the ESP32 networking functionality.

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "esp32.h"
#include "spi.h"
#include "cpu.h"

#define DELAY           20000000
#define RECV_BUF_LEN    4096
#define VERBOSITY       1

#define EXPECT_NOISY    0
#define EXPECT_SILENT   1

#define MSG_OK          "\r\nOK\r\n", 6
#define MSG_BUSY        "\r\nbusy p...\r\n", 13
#define MSG_C           "C\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 67

#define min(x,y) ((x) < (y) ? (x) : (y))

static char recv_buf[RECV_BUF_LEN];
static uint32_t recv_len;

struct esp32_event_queue_element {
	char *data;
	uint32_t len;
	struct esp32_event_queue_element *next;
};

struct esp32_event_queue_element *esp32_event_queue_head = NULL;
struct esp32_event_queue_element *esp32_event_queue_tail = NULL;

enum esp32_event esp32_classify_event(char *ptr, uint32_t len);

// Pushes an event message to the back of the event queue
void esp32_push_event_to_queue(char *ptr, uint32_t len) {
	char *new_ptr = malloc(len);
	if(new_ptr == NULL) {
		// Drop packet if memory allocation fails
		return;
	}

	memcpy(new_ptr, ptr, len);

	struct esp32_event_queue_element *new_elem = malloc(sizeof(struct esp32_event_queue_element));
	if(new_elem == NULL) {
		free(new_ptr);
		// Drop packet if memory allocation fails
		return;
	}

	new_elem->data = new_ptr;
	new_elem->len = len;
	new_elem->next = NULL;

	if(esp32_event_queue_tail == NULL) {
		esp32_event_queue_head = new_elem;
		esp32_event_queue_tail = new_elem;
	} else {
		esp32_event_queue_tail->next = new_elem;
		esp32_event_queue_tail = new_elem;
	}
}

// Pops an event message from the front of the queue
enum esp32_result esp32_pop_event_from_queue(char **ptr, uint32_t *len) {
	if(esp32_event_queue_head == NULL) {
		return ESP32_ERR;
	}

	struct esp32_event_queue_element *old_head = esp32_event_queue_head;
	esp32_event_queue_head = old_head->next;
	if(old_head->next == NULL) {
		esp32_event_queue_tail = NULL;
	}

	*ptr = old_head->data;
	*len = old_head->len;
	free(old_head);

	return ESP32_OK;
}

// Receives new data from the ESP32, if data is ready.
enum esp32_result esp32_recv(void) {
	while(spi_read_ready()) {
		spi_recv(recv_buf, sizeof(recv_buf), &recv_len);

#if VERBOSITY > 1
		if(recv_len > 2 && recv_buf[0] == '\r' && recv_buf[1] == '\n') {
			printf("<-- \\r\\n%s\r\n", &recv_buf[2]);
		} else {
			printf("<-- %s\r\n", recv_buf);
		}
#endif

		if(esp32_classify_event(recv_buf, recv_len) != ESP32_UNKNOWN_EVENT) {
			esp32_push_event_to_queue(recv_buf, recv_len);
			continue;
		}

		return ESP32_OK;
	}

	return ESP32_ERR;
}

// Checks whether the expected value and length was received, without performing a new receive operation.
enum esp32_result esp32_check_expectation(int how, const char *expected_value, uint32_t expected_len) {
	if(strncmp(expected_value, recv_buf, min(expected_len, recv_len)) != 0 || recv_len != expected_len) {
#if VERBOSITY > 0
		if(how == EXPECT_NOISY) {
			printf("Got an unexpected reply from ESP32:\r\n");

			printf("  Expected length: %u\r\n", expected_len);
			printf("  Actual length:   %u\r\n", recv_len);

			printf("  Expected (hex): ");
			for(uint32_t i = 0; i < expected_len; ++i) {
				printf("%02x ", expected_value[i]);
			}
			printf("\r\n");
			printf("  Data (hex):     ");
			for(uint32_t i = 0; i < recv_len; ++i) {
				printf("%02x ", recv_buf[i]);
			}
			printf("\r\n");

			if(expected_value[0] == '\r' && expected_value[1] == '\n') {
				printf("  Expected (string): \\r\\n%s\r\n", &expected_value[2]);
			} else {
				printf("  Expected (string): %s\r\n", expected_value);
			}
			if(recv_buf[0] == '\r' && recv_buf[1] == '\n') {
				printf("  Data (string):     \\r\\n%s\r\n", &recv_buf[2]);
			} else if(recv_len == 0) {
				printf("  Data (string):     <no data>\r\n");
			} else {
				printf("  Data (string):     %s\r\n", recv_buf);
			}
		}
#endif

		return ESP32_ERR;
	}

	return ESP32_OK;
}

// Performs a receive operation and checks if the expected value was returned.
enum esp32_result esp32_expect(const char *expected_value, uint32_t expected_len) {
	if(esp32_recv() == ESP32_ERR) {
		recv_len = 0;
		return esp32_check_expectation(EXPECT_NOISY, expected_value, expected_len);
	}

	return esp32_check_expectation(EXPECT_NOISY, expected_value, expected_len);
}

enum esp32_result esp32_init(void) {
	// Reset the ESP32
    spi_send("AT+RST\r\n");

    // Wait for it to process the reset
    delay(DELAY);

    // Disable command echoing
    spi_send("ATE0\r\n");

    // Check that the results are the expected values
    if(esp32_expect("ATE0\r\n", 6) == ESP32_ERR ||
    		esp32_expect(MSG_OK) == ESP32_ERR) {
    	return ESP32_ERR;
    }

    return ESP32_OK;
}

enum esp32_result esp32_connect_to_ap(const char *ssid, const char *pwd) {
	// Set ESP32 into station mode
	spi_send("AT+CWMODE=1\r\n");

	// Check the result of the command
	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	// Construct the message to join the access point
	uint32_t buf_len = strlen(ssid) + strlen(pwd) + 20;
	char join_msg[buf_len];
	snprintf(join_msg, buf_len, "AT+CWJAP=\"%s\",\"%s\"\r\n", ssid, pwd);

	// Join the AP
	spi_send(join_msg);

	// We expect three messages in return if everything goes well:
	// - WIFI CONNECTED
	// - WIFI GOT IP
	// - OK
	//
	// These messages take some time to appear however, so we just try receiving messages until they arrive.
	while(esp32_recv() == ESP32_ERR);
	if(esp32_check_expectation(EXPECT_NOISY, "WIFI CONNECTED\r\n", 16) == ESP32_ERR) {
		return ESP32_ERR;
	}

	while(esp32_recv() == ESP32_ERR);
	if(esp32_check_expectation(EXPECT_NOISY, "WIFI GOT IP\r\n", 13) == ESP32_ERR) {
		return ESP32_ERR;
	}

	while(esp32_recv() == ESP32_ERR);
	if(esp32_check_expectation(EXPECT_NOISY, MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	return ESP32_OK;
}

enum esp32_result esp32_disconnect_from_ap(void) {
	// Try to close the connection until a disconnection message occurs
	do {
		spi_send("AT+CWQAP\r\n");

		while(esp32_recv() != ESP32_ERR) {
			if(esp32_check_expectation(EXPECT_SILENT, "WIFI DISCONNECT\r\n", 17) == ESP32_OK) {
				break;
			}
		}
	} while(esp32_check_expectation(EXPECT_SILENT, "WIFI DISCONNECT\r\n", 17) == ESP32_ERR);

	// Soak up extra messages generated by the request
	while(esp32_recv() != ESP32_ERR) {}

	// Put the ESP32 into power saving mode
	spi_send("AT+CWMODE=0\r\n");
	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	return ESP32_OK;
}

enum esp32_result esp32_open_server(uint16_t port) {
	// Opening a server requires multiplexing to be enabled
	spi_send("AT+CIPMUX=1\r\n");
	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	// Construct the message to join
	char open_msg[35]; // Contains some extra room in case SSL is desired later on
	snprintf(open_msg, sizeof(open_msg), "AT+CIPSERVER=1,%u\r\n", port);

	// Open the server
	spi_send(open_msg);
	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	return ESP32_OK;
}

// Classifies an event from an ESP32 message
enum esp32_event esp32_classify_event(char *ptr, uint32_t len) {
	if(strncmp(ptr, "\r\n+IPD,", min(len, 7)) == 0) {
		return ESP32_MSG_RECEIVED;
	}
	// The id is guaranteed to be a 1-digit number and thus we know the length exactly
	if(len == 11 && strncmp(&ptr[2], "CONNECT\r\n", min(len - 2, 9)) == 0) {
		return ESP32_NEW_CONNECTION;
	}
	// The id is guaranteed to be a 1-digit number and thus we know the length exactly
	if(len == 10 && strncmp(&ptr[2], "CLOSED\r\n", min(len - 2, 8)) == 0) {
		return ESP32_CLOSED_CONNECTION;
	}
	return ESP32_UNKNOWN_EVENT;
}

// Receives one of the possible event types and fills the values pointed to by its parameters appropriately.
// The caller is responsible for calling free on the value pointed to by the data pointer, if data is not null.
enum esp32_event esp32_wait_for_event(uint32_t *id, uint32_t *len, char **data) {
	char *buf;
	uint32_t whole_msg_len;
	while(esp32_pop_event_from_queue(&buf, &whole_msg_len) != ESP32_OK) {
		esp32_recv();
	}

	switch(esp32_classify_event(buf, whole_msg_len)) {
	case ESP32_MSG_RECEIVED:
		// The message format is "\r\n+IPD,<id>,<length>:<msg>"
		if(id != NULL) {
			*id = buf[7] - '0';
		}
		uint32_t computed_len;
		if(len != NULL) {
			computed_len = strtol(&buf[9], data, 10);
			*len = computed_len;
		}
		if(data != NULL) {
			// data points to the ":" here, so we increase it to point to the message instead
			*data += 1;
			if(whole_msg_len - (*data - buf) < computed_len) {
				*data = NULL;
				free(buf);
				return ESP32_MSG_RECEIVED;
			}
			// Copy the data to a new location, so the caller can call free on it.
			// If we didn't do this, we would return a pointer into the middle of a buffer, which cannot be freed directly.
			char *new_location = malloc(computed_len);
			if(new_location != NULL) {
				memcpy(new_location, *data, computed_len);
				*data = new_location;
			} else {
				*data = NULL;
			}
			free(buf);
		} else {
			// if the caller is not interested in the data, we can simply free it
			free(buf);
		}
		return ESP32_MSG_RECEIVED;
	case ESP32_NEW_CONNECTION:
		if(id != NULL) {
			*id = buf[0] - '0';
		}
		// there is no meaningful data to return here, so we free the buffer
		free(buf);
		if(len != NULL) {
			*len = 0;
		}
		if(data != NULL) {
			*data = NULL;
		}
		return ESP32_NEW_CONNECTION;
	case ESP32_CLOSED_CONNECTION:
		if(id != NULL) {
			*id = buf[0] - '0';
		}
		// there is no meaningful data to return here, so we free the buffer
		free(buf);
		if(len != NULL) {
			*len = 0;
		}
		if(data != NULL) {
			*data = NULL;
		}
		return ESP32_CLOSED_CONNECTION;
	}

	if(data != NULL) {
		*data = buf;
	}
	if(len != NULL) {
		*len = whole_msg_len;
	}
	return ESP32_UNKNOWN_EVENT;
}

enum esp32_result esp32_send(uint32_t id, uint32_t len, char *data) {
	// Inform the ESP32 that we wish to send a message
	uint32_t buf_len = 40;
	char send_msg[buf_len];
	snprintf(send_msg, buf_len, "AT+CIPSEND=%d,%d\r\n", id, len);
	spi_send(send_msg);

	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}
	if(esp32_expect("\r\n>", 3) == ESP32_ERR) {
		return ESP32_ERR;
	}
	// Actually send the data
	spi_send_with_len(data, len);

	buf_len = 35;
	char recv_msg_buf[buf_len];
	snprintf(recv_msg_buf, buf_len, "\r\nRecv %d bytes\r\n", len);
	esp32_expect(recv_msg_buf, strlen(recv_msg_buf));

	// Usually the ESP32 will send a SEND OK message here, but there appear to be some situations in which
	// that message is not sent, so we check it here, but don't error if it doesn't exist.
	if(esp32_recv() == ESP32_OK) {
		if(esp32_check_expectation(EXPECT_NOISY, "\r\nSEND OK\r\n", 11) == ESP32_ERR) {
			return ESP32_ERR;
		}
	}

	return ESP32_OK;
}

enum esp32_result esp32_close_connection(uint32_t id) {
	// Build the message to close the connection
	uint32_t buf_len = 30;
	char send_msg[buf_len];
	snprintf(send_msg, buf_len, "AT+CIPCLOSE=%d\r\n", id);
	spi_send(send_msg);

	// Sometimes the ESP32 sends no message here (but with extreme delay), so we also accept no message.
	if(esp32_recv() == ESP32_OK) {
		// After trying it a couple of times, it became apparent that sometimes an error is returned here.
		// The connection will still appear to be closed on the ESP32 (at least the ID is reused).
		// This is likely because the other party disconnected first.
		// Because of this we accept both OK and ERROR messages here.
		if(esp32_check_expectation(EXPECT_SILENT, MSG_OK) != ESP32_OK && esp32_check_expectation(EXPECT_SILENT, "\r\nERROR\r\n", 9) != ESP32_OK) {
			// We did not get the expected message, now we just do a noisy comparison to report the error.
			esp32_check_expectation(EXPECT_NOISY, MSG_OK);
			return ESP32_ERR;
		}
	}

	return ESP32_OK;
}

enum esp32_result esp32_close_server(void) {
	spi_send("AT+CIPSERVER=1\r\n");

	if(esp32_expect(MSG_OK) == ESP32_ERR) {
		return ESP32_ERR;
	}

	return ESP32_OK;
}
