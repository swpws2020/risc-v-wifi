#ifndef HTTP_HELPERS_H_
#define HTTP_HELPERS_H_

#include <stdint.h>

// prepares an http response with the given content and status code
// the returned buffer should be freed by the caller
char *http_prepare_response(char *content, uint32_t content_len, uint16_t status_code, uint32_t *out_len);

#endif /* HTTP_HELPERS_H_ */
