---
title: RISC-V
subtitle: Präsentation des Softwareprojekts
linkcolor: blue
date: 02.03.2021
author:
- Felix Manuel Peterka
- Niklas Pauli
- Niclas Schwarzlose
---

# Team

- Felix Manuel Peterka (fptk@zedat.fu-berlin.de)
- Niklas Pauli (niklap97@zedat.fu-berlin.de)
- Niclas Schwarzlose (nischw@zedat.fu-berlin.de)

GitLab-Repo: [https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi)
ReportingPad: [https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/boards](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/boards)
Erhaltene Hardware:

- 4x SiFive HiFive1 Rev B
- 1x Xilinx Arty 7
- 1x ARM-USB-TINY-H Debugger
- Einige Kabel


# Inhalt

- Einleitung
- Verwendung des Arty 7 FPGA
- Der Webserverstack anhand des HiFive1 Rev B
- Entwicklung des Angriffs
- Der Angriff auf dem Arty
- Multizone Security
- OpenMZ
- Multizone auf dem HiFive
- Einschätzung der Projektarbeit

# Überblick 
- Multizone erlaubt verschiedene Softwaremodule mithilfe von Hardware zu trennen
- Wir wollen RISC-V Hardware verwenden um den Nutzen von Multizone aufzuzeigen
- Wir wollen einen Angriff entwickeln, der normalerweise funktioniert, jedoch durch Multizone gestoppt werden kann
    - Dieser soll in ein realistisches Szenario eingebettet werden
- Testen mit unterschiedlicher Hardware (SiFive HiFive1 Rev B & Xilinx Arty 7 FPGA)

# Vorgehen
- HiFive hat bereits RISCV Instruction Set - nicht frei programmierbar
- Instruktionssatz auf Arty (FPGA) programmieren -> via mcs 
- HexFive (Multizone) bietet offenes Image hierfür an (X300, basiert auf E300)
- Notwendig: Xilinx eigene IDE 

# MCS flashen und Multizone-sdk
- Arty braucht Xilinx-IDE - Xilinx IDE braucht exakte Ubuntu Version
- Multizone-sdk ist über git verfügbar
- Mit vorkompilierter HexFive-RISCV-Toolchain mcs bauen
- Problem: Multizone braucht Abhängigkeiten aus Ubuntu 20.04
- Folge: Dependency Hell + Ubuntu auto update
- Lösung: Erst mcs bauen und flashen, dann Multizone Abhängigkeiten installieren
(und ubuntu auto update deaktivieren)

# Programm portieren
- selbes Instruction Set, selbes Programm
- Problem 1: Arty hat kein Wifi: Multizone-iot-sdk und lwIP statt esp32
- Problem 2: Freedom Studio hat eigene Toolchain, HexFive Toolchain in FS einbinden
- Problem 3: Debugger funktioniert nicht, debuggen (und Adressen finden) via printf




# HiFive1 Rev B Aufbau

![Das HiFive1 Rev B](board.png){width=225pt}


# Kommunikation mit ESP32

Erfolgt über AT Befehle:

\color{blue}
```text
> AT+CWMODE=1
```
\color{black}
```text
< OK
```
\color{blue}
```text
> AT+CWJAP="AP Name","AP Passwort"
```
\color{black}
```text
< WIFI CONNECTED
< WIFI GOT IP
< OK
```

Die AT Befehle und Antworten werden über eine SPI-Schnittstelle ausgetauscht.


# Webserver

- Das Packet wird per `+IPD` Nachricht vom ESP32 empfangen.
- Die Anfrage wird dann in `webserver_handle_request` geparsed.
- `server_request_handler` stellt dann je nach Anfrage die richtige Antwort zusammen.
- Dabei wird `generate_page` verwendet, unsere Funktion mit der Schwachstelle.
- `generate_page` unterscheidet anhand eines Namens zwischen einem Admin und anderen Nutzern.

Das Ziel des Angriffs ist es, "Adminzugriff" zu bekommen.

- Wir wollen also den Adminnamen herausfinden.


# Angriffsentwicklung

- Für die Entwicklung des Angriffs haben wir alle uns verfügbaren Mittel (z.B. Disassembly) genutzt.
- Auch wenn sie einem Angreifer in einem realistischen Szenario nicht zur Verfügung stehen würden.
- Der Angriff ist jedoch auch ohne diese Mittel möglich. Es wäre nur viel schwerer ihn zu finden.


# Die Schwachstelle

Ein Buffer mit fester Größe wird mit beliebig vielen Bytes gefüllt.

```c
char message[128];
//   ^^^^^^^^^^^^
// 128-Byte Buffer auf dem Stack
memcpy(message, "Hallo, ", 7);
memcpy(message + 7, name, name_len);
//                        ^^^^^^^^
//               Buffer Overflow, wenn > 120
message[7 + name_len] = '\0';
```

- Angriffsidee: Maschinencode in `message` schreiben und in diesen Maschinencode springen.


# Der Angriff

Der Stackframe von `generate_page`:

\begin{tabular}{|c|c|c|c|c|c|}
    \hline
    sp + 28 & ... & sp + 155 & ... & sp + 168 & sp + 172 \\ \hline
    \multicolumn{3}{|c|}{message} & ... & frame pointer & return address \\
    \hline
\end{tabular}

- Wir können die Rücksprungadresse überschreiben, wenn wir mindestens 148 Bytes in `message` schreiben.
- Müssen aber dabei den alten Framepointer unverändert lassen, damit der Stackframe des Aufrufers korrekt funktioniert.


# Für den Angriff relevante Informationen

- Die Adresse des ersten Maschinencodebefehls in `message` (`printf("%p\n", &message[8])`)
- Der gespeicherter Framepointer des alten Stackframes (`printf("0x%x\n", *(uint32_t *)&message[140]`)
- Die ursprüngliche Rücksprungadresse von `generate_page` (über Disassembly)
- Die Adresse des Adminnamen zum "zurückgeben von `generate_page`" (über Disassembly)

Python-Skript stellt die Payload für den Angriff aus allen relevanten Informationen zusammen.



# Der Angriff auf dem Arty
- Unterschied: Zwei Zonen, Server Zone 1, Abfrage Zone 2
- Server schickt name via MultiZone Message (je 16 Byte)
- Separate Speicherbereiche sorgen für Load Access Fault beim auslesen

# Multizone Security
- bietet eine durch Hardware erzwungene, Software definierte Trennung für mehrere Domänen
- volle Kontrolle über Daten, Programme und Peripheriegeräte
- keine zusätzlichen IP-Blöcke
- keine Änderungen an der bestehenden Firmware erforderlich
- ist Open Source und für die kommerzielle Nutzung lizenzfrei

# Hardwareanforderungen
- zertifiziert für jeden RISC-V-Kern , mit:
    - RV32I, RV32E, RV64I Architektur
    - 'U'-Erweiterung (Benutzermodus)
    - Mindestens 4 PMP-Register
    - 4KB FLASH und 1KB RAM

# Features
- getrennte vertrauenswürdige Ausführungsumgebungen (Zonen) 
- Mehrere memory-mapped Ressourcen pro Zone - z. B. Flash, Ram, E/A, UART, Irqs, etc.
- Beliebige Kombination von Lese-, Schreib- und Ausführungsrichtlinien 
- Ressourcenüberlappung erlaubt, aber nicht empfohlen
- Präemptiver Scheduler: round robin, konfigurierbarer tick
- Interzonen-Kommunikation basierend auf Messaging - kein Shared Memory
- vollständig konfigurierbare Zonen-/Interrupt-Zuordnung

# multizone.cfg
\footnotesize
```
# MultiZone reserved memory: 6K @0x20010000, 6K @0x80000000

Tick = 10 # ms

Zone = 1
   #irq  = 3 # DMA
    plic = 3 # UART
    base = 0x20012000; size =      32K; rwx = rx # FLASH 
    base = 0x80001800; size =       4K; rwx = rw # RAM
    base = 0x10013000; size =    0x100; rwx = rw # UART

Zone = 2
   #irq  = 20, 21, 22 # BTN0 BTN1 BTN2 (CLINT)
    base = 0x2001A000; size =    8K; rwx = rx # FLASH
    base = 0x80002800; size =    2K; rwx = rw # RAM
    base = 0x10025000; size = 0x100; rwx = rw # PWM LED
    base = 0x10012000; size = 0x100; rwx = rw # GPIO
```
\normalsize

# OpenMZ
- läuft Multizone auch auf dem Hifive1 rev b?
    - besitzt eigentlich die nötigen Hardwarevoraussetzungen
- Fund: 
    - Masterarbeit: "OpenMZ: a C implementation of the MultiZone API" 
- vollständige Open-Source-Implementierung der MultiZone-API
- schlanker als Multizone Security (Codebasis von etwa 800 Zeilen)

# Multizone auf dem HiFive
- vor ein paar Wochen: Multizone SDK erhält Hifive1 rev b BSP
- Multizone läuft ohne Einschränkungen auf dem Hifive
- Multizone verspricht: 
    - schnelle und einfache Integration in andere Software
- Unsere Erfahrung:  
    - Integrieren in Software komplex
    - viele Anpassungen nötig 
    - Fachwissen erforderlich

# Einschätzung der Projektarbeit
Negativ:
- remote Arbeit erschwerte grundsätzlich die Kommunikation und Lösungsfindung
- Dokumentation könnte ausführlicher sein

Positiv:
- regelmäßige Treffen 
- schneller Austausch
- gegenseitige Unterstützung und Hilfsbereitschaft
- Teamgeist

-> es hat Spaß gemacht!
