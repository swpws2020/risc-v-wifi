# RISC-V und Multizone

Der Großteil der Dokumentation für dieses Projekt ist im [Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/home) zu finden.

Einen guten ersten Überblick über das Projekt findet man in den Präsentationen.

## Was die Ordner und Dateien bedeuten

- `ArtyImplementation`: Hier befindet die Softwarevariante, die auf einem Arty 7 FGPA läuft ([Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/home#arty-7)).
- `multizone_wifi`: Dies ist die Softwarevariante für das HiFive1 Rev B mit dem offiziellen Multizone SDK (momentan nicht funktionstüchtig) ([Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/home#multizone)).
- `openMZ`: Dies ist die Softwarevariante für das HiFive1 Rev B mit der OpenMZ Implementierung der Multizone API ([Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/OpenMZ)).
- `presentations`: Dieser Ordner enthält die Folien für alle Präsentationen die wir zu diesem Softwareprojekt gehalten haben.
- `sifive_software`: Diese Variante der Software ist für das HiFive1 Rev B ohne Multizone ([Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/home#hifive-1)).
- `assembler.py`: Ein Wrapper, der den RISC-V assembler aufruft und einen Angriff entwickelt ([Wiki](https://git.imp.fu-berlin.de/swpws2020/risc-v-wifi/-/wikis/HiFive-Angriff-(ohne-MultiZone))).
- `create_hifive_attack.sh`: Ein Skript, welches den Python-Wrapper mit den korrekten Adressen für den Angriff (der Variante HiFive ohne MZ) aufruft (commit d2b7fb6fafe40de05dd75b1e889c4fb880f795a4).
