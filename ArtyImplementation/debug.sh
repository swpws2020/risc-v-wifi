#!/bin/sh

make && make load

if [ -n $OPENOCD ]; then
    $OPENOCD/bin/openocd -c "gdb_port 3333" -c "telnet_port 4444" -c "tcl_port 6666" -c "set protocol cjtag"\
        -c "set connection probe" -f bsp/X300/openocd.cfg -d3 > openocd-debug.log 2>&1 &
else
    echo "OPENOCD not set"
    exit
fi

echo "$(ps -A | grep openocd)"
#wait for openocd
sleep 1

if [ -n $RISCV ]; then
    $RISCV/bin/riscv64-unknown-elf-gdb zone1/zone1.elf -ex "target extended-remote localhost:3333"
else
    echo "RISCV not set"
    exit
fi
