# Getting Started
## was hier passiert (ist)
Hier liegt die Implementierung des Angriffs auf dem Arty.
Ein Großteil des Codes liegt in zone1/
Lediglich für die Verhinderung des Angriffs (ArtyProhibitAttack) wurde in zone2/ gearbeitet - hier gibt es zudem den Ordner shared, in dem Dateien liegen, deren Code von beiden Zonen benutzt wird.

bsp/X300 - hier liegt multizone.cfg sowie die Konfigurationsdatei für openocd, sollte man diese benötigen.

ext/ - hier liegen die Bibliotheken für riscv (metal etc.) sowie lwip, die Bibliothek die für die TCP/IP Verbindung genutzt wird.

## Branches
Es gibt insgesamt 3 branches:
- master: der master branch.
- ArtyProhibitAttack: In diesem Branch wird der Angriff verhindert. Die Überprüfung des Zugangs/ Adminnamens wird in Zone 2 überprüft.
- ArtyEnableAttack: In diesem Branch ist der Angriff implementiert und funktioniert über Zone 1.

## Toolchain
Zum Bauen wird die Toolchain aus der multizone sdk bzw. die [Toolchain von hexfive](https://hex-five.com/wp-content/uploads/riscv-gnu-toolchain-20200613.tar.xz)

## Basisrepo
Dieser Teil des Projekts basiert auf dem [multizone-iot-sdk](https://github.com/hex-five/multizone-iot-sdk). Den genauen commit wissen wir nicht mehr, aber zum Aktuellen Zeitpunkt fanden alle commits auf den master zwischen dem 15-17 Dezember 2020 statt. Es ist daher ziemlich wahrscheinlich, dass wir mit dem neusten commit (März 2021) gearbeitet haben: 0768c2b1170e87bceec569d94fa6ca3ad2eedd44
