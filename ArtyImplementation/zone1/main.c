/* Copyright(C) 2020 Hex Five Security, Inc. - All Rights Reserved */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "lwip/dhcp.h"
#include "lwip/dns.h"
#include "lwip/init.h"
//network interface headers
#include "lwip/netif.h"
#include "lwip/timeouts.h"
//network interface headers for ethernet
#include "netif/ethernet.h"
#include "lwip/apps/sntp.h"

#include "lwip/altcp_tcp.h"

//multizone api
#include "multizone.h"
#include "platform.h"
//hex-five code for the ethernet link on Arty
#include "xemaclite.h"
#include "queue.h"

#include "tcp_cb.h"
#include "message_handling.h"

// ----------------------------------------------------------------------------

/* lwIP system time (ms) - no date/time, just delta */
u32_t sys_now(void) {

	return (u32_t) (MZONE_RDTIME() * 1000 / RTC_FREQ);
}

/* mtime zero epoque (sec) - updated via sntp */
static time_t mtime_zero_epoque = 0;

/* set mtime zero epoque to avoid changes to mtime/mtimecmp (i.e. FreeRTOS tick) */
void sntp_set_system_time(uint32_t t){
	mtime_zero_epoque = t - (time_t) (MZONE_RDTIME() / RTC_FREQ);
}

/* mbedtls system time (sec) - actual date/time for X509 exp verification */
time_t time(time_t *timer) {

	time_t lt = mtime_zero_epoque + (time_t) (MZONE_RDTIME() / RTC_FREQ);

	if (timer != NULL) {

		*timer = lt;

	}

	return lt;

}


// ----------------------------------------------------------------------------



static struct netif netif;
static struct queue *rx_queue;

__attribute__((interrupt())) void trp_handler(void)  { // non maskable traps

	const unsigned long mcause = MZONE_CSRR(CSR_MCAUSE);
	const unsigned long mepc   = MZONE_CSRR(CSR_MEPC);
	const unsigned long mtval  = MZONE_CSRR(CSR_MTVAL);

/*	switch(mcause){

	case 0 : printf("Instruction address missaligned : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 1 : printf("Instruction access fault : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 2 : printf("Illegal instruction : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 3 : printf("Breakpoint : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 4 : printf("Load address missaligned : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 5 : printf("Load access fault : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 6 : printf("Store/AMO address missaligned : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
	 	 	 break;

	case 7 : printf("Store access fault : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 8 : printf("Environment call from U-mode : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 9 : printf("Environment call from S-mode : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	case 11: printf("Environment call from M-mode : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);
			 break;

	default : printf("Exception : 0x%08x 0x%08x 0x%08x \n", mcause, mepc, mtval);

	}*/

	asm("1: j 1b"); // TBD restart this zone?

}
__attribute__((interrupt())) void msi_handler(void)  { // machine software interrupt (3)
	asm volatile("ebreak");
}
__attribute__((interrupt())) void tmr_handler(void)  { // machine timer interrupt (7)
	 MZONE_WRTIMECMP((uint64_t)-1); // clear mip.7
}
__attribute__((interrupt())) void plic_handler(void) { // machine external interrupt (11)

	const uint32_t plic_int = PLIC_REG(PLIC_CLAIM_OFFSET); // PLIC claim

	struct pbuf *p;
	// check both rx buffers as they share the same irq
	while ( (p = xemaclite_input(&netif)) !=NULL){
		if (!queue_is_full(rx_queue))
			queue_put(rx_queue, p);
		else {
			pbuf_free(p);
		}
	}

	PLIC_REG(PLIC_CLAIM_OFFSET) = plic_int; // PLIC complete

}



//The flags in lwip/opts.h need to be 1, otherwise these CBs won't be called
static void netif_link_callback(struct netif *netif) {

	printf2("netif_link_callback: %s\n", netif_is_link_up(netif) ? "up" : "down");

}

static void netif_status_callback(struct netif *netif){

	printf2("netif_status_callback: address %s\n", ipaddr_ntoa(netif_ip4_addr(netif)));
}

static void dns_callback(const char *name, const ip_addr_t *addr, void *arg ){

	printf2("dns_callback: %s %s\n", name, addr ? ipaddr_ntoa(addr) : "<not found>");

	ip_addr_copy(*(ip_addr_t *)arg, *addr);

}

static void lwip_thread(){

	/* Check for received frames, feed them to lwIP */
	while (1){

		CSRC(mie, 1<<11);
		struct pbuf* p = queue_get(rx_queue);
		CSRS(mie, 1<<11);

			if(p != NULL) {

				if(netif.input(p, &netif) != ERR_OK)
					pbuf_free(p);

			} else

				break;

	}

	/* lwIP background tasks ( 250ms, 500ms) */
	sys_check_timeouts();

	/* set MultiZone timer and go to sleep */
	const unsigned int sleeptime = sys_timeouts_sleeptime();
	if (sleeptime != SYS_TIMEOUTS_SLEEPTIME_INFINITE)
		MZONE_ADTIMECMP( (uint64_t)(sleeptime * RTC_FREQ/1000));
}

int main(void) {
	// vectored trap handler
	static __attribute__ ((aligned(4)))void (*trap_vect[32])(void) = {};
	trap_vect[0]  = trp_handler;
	trap_vect[3]  = msi_handler;
	trap_vect[7]  = tmr_handler;
	trap_vect[11] = plic_handler;


	//enabling trap handling via vector
	CSRW(mtvec, trap_vect);	CSRS(mtvec, 0x1);

	//interrupt enabling
    CSRS(mie, 1<<11); 		// enable external interrupts (PLIC)
    CSRS(mie, 1<<7); 		// enable timer (TMR)
    CSRS(mstatus, 1<<3);	// enable global interrupts (PLIC, TMR)

	// Enable XEMACLITE RX IRQ (PLIC Priority 1=lowest 7=highest)
	PLIC_REG(PLIC_PRI_OFFSET + (PLIC_XEMAC_RX_SOURCE << PLIC_PRI_SHIFT_PER_SOURCE)) = 1;
	PLIC_REG(PLIC_EN_OFFSET) |= 1 << PLIC_XEMAC_RX_SOURCE;

	/* IP Stack Init */
	lwip_init();

	/* init ethernet interface */
	rx_queue = queue_new(16);

    netif_add(&netif, IP4_ADDR_ANY, IP4_ADDR_ANY, IP4_ADDR_ANY, NULL, xemaclite_init, ethernet_input);

	netif_set_default(&netif);
	netif_set_up(&netif);
	netif_set_status_callback(&netif, netif_status_callback);
	netif_set_link_callback(&netif, netif_link_callback);

	// Poll ethernet link
	while(!netif_is_link_up(&netif)){
		xemaclite_poll_link_state(&netif);
		lwip_thread();
		MZONE_WFI();
	}

	// Wait for dhcp settings
	dhcp_start(&netif);
	while (!dhcp_supplied_address(&netif)){
		lwip_thread();
		MZONE_WFI();
	}


	// Main loop (link is up, dhcp bound, broker addr resolved)
	// Define lwIP connection structures

	struct altcp_pcb *package_control_block;
	package_control_block = NULL;
    tcp_cb_init(package_control_block);

	while (1) {

        MZONE_WFI(); // resume upon 1) timer exp 2) eth irq 3) incoming msg

		lwip_thread();

		xemaclite_poll_link_state(&netif);


	}
	return 0;
}
