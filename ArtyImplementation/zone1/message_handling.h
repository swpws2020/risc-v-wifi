#ifndef ZONE1_MESSAGE_HANDLING_H_
#define ZONE1_MESSAGE_HANDLING_H_

//taken from the multizone-iot-sdk source
#include "multizone.h"

#define printf2(format, args...) { /* Multi-part printf()  */\
	char *str = malloc(128);									 \
	if (str != NULL){										 \
		const int len = snprintf(str, 128, format, ## args);	 \
		for (int i=0; i<(len/16)+1; i++){					 \
			while(!MZONE_SEND(2, (str+16*i))) MZONE_YIELD(); \
		}													 \
		free(str);											 \
	}														 \
}															 \


static int msg_handler(const int zone, const char *msg);

#endif /* ZONE1_MESSAGE_HANDLING_H_ */
