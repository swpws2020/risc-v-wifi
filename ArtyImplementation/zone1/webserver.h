#ifndef WEBSERVER_H_
#define WEBSERVER_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct network_implementation {
	// Returns non-zero value if the network implementations could not be initialized successfully.
	int (*init)(void *custom_data);
	// Note that the webserver will call free on the data pointer after calling this function.
	enum network_event (*recv_data_or_connection)(void *custom_data, int *connection_id, uint32_t *len, char **data);
	// Returns non-zero value if the data could not be sent successfully.
	int (*send_data)(void *custom_data, int connection_id, uint32_t len, char *data);
	// Returns non-zero value if the connection could not be closed successfully.
	int (*close_connection)(void *custom_data, int connection_id);
	// Allows implementations to maintain associated data.
	void *custom_data;
};

enum network_event {
	NEW_CONNECTION,
	CLOSED_CONNECTION,
	DATA_RECEIVED,
	ERROR,
};

enum request_type {
	GET_REQUEST,
	POST_REQUEST
};

// A function prototype to handle a request.
// When implementing handlers, please note that the data pointer is not guaranteed to be zero-terminated.
typedef char *(*request_handler)(char *location, enum request_type type, char *data, uint32_t data_len, uint32_t *out_len, bool *free_result);

// Returns non-zero value if the server could not be properly initialized.
int webserver_init(struct network_implementation network);
// Runs the webserver with the given request handling function.
void webserver_run(request_handler handler);
// Percent decodes the given data in-place returning the new length.
// Also appends a '\0'-byte, if the output is shorter than the input.
uint32_t webserver_percent_decode(char *data, uint32_t len);

#endif /* WEBSERVER_H_ */
