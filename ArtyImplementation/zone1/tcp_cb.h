/*
 * tcp_cb.h
 *
 *  Created on: Jan 8, 2021
 *      Author: fptk
 */

#ifndef ZONE1_TCP_CB_H_
#define ZONE1_TCP_CB_H_

#include "lwip/err.h"
//for some reason gcc cannot find the err_t type definition in err.h

#include "lwip/altcp.h"

//mainly from the tcpecho_raw lwIP contrib example by Christian Simons


enum tcp_cb_states {
	TCP_NONE = 0,
	TCP_ACCEPTED,
	TCP_RECEIVED,
	TCP_CLOSING
};

struct tcp_cb_state
{
	uint8_t state;
	uint8_t retries;
	struct tcp_pcb *package_control_block;

	struct pbuf *package_buffer;
};
enum tcp_cb_dbg
{
	DBG_OK =0,
	DBG_ACCEPT,
	DBG_RECV,
	DBG_CLOSE,
	USED_PORT,
	PCB_INVALID,
	FAILED_INIT_MEM
};
static void tcp_cb_free(struct tcp_cb_state *tcp_state);
static void tcp_cb_close(struct altcp_pcb *package_control_block, struct tcp_cb_state *tcp_state);
static void tcp_cb_send(struct altcp_pcb *package_control_block, struct tcp_cb_state *tcp_state);
static err_t tcp_cb_sent(void *arg, struct altcp_pcb *package_control_block, uint16_t len);
static void tcp_cb_error(void *arg, err_t err);
static err_t tcp_cb_poll(void *arg, struct altcp_pcb *package_control_block);
static err_t tcp_cb_recv(void *arg, struct altcp_pcb *package_control_block, struct pbuf *package_buffer
    , err_t recv_error);
static err_t tcp_cb_accept(void *arg, struct altcp_pcb *new_package_control_block, err_t accpet_error);
void tcp_cb_init(struct altcp_pcb *package_control_block);


#endif /* ZONE1_TCP_CB_H_ */
