/*
 * arty_impl.h
 *
 *  Created on: Jan 18, 2021
 *      Author: fptk
 */

#ifndef ZONE1_ARTY_IMPL_H_
#define ZONE1_ARTY_IMPL_H_

#include "webserver.h"

struct network_implementation ARTY_NETWORK_IMPLEMENTATION;

#endif /* ZONE1_ARTY_IMPL_H_ */
