/*
 * tcp_cb.c
 * mainly taken from tcpecho_raw example of the lwip contrib repo
 *
 *  Created on: Jan 8, 2021
 *      Author: fptk
 */



#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "tcp_cb.h"
#include "server_utility.h"


static err_t tcp_cb_parse_http(struct altcp_pcb *package_control_block, struct tcp_cb_state *tcp_state, struct pbuf *package_buffer)
{
	/* basically the same as in webserver.c
	 * may needs to be fitted towards several buffers in buffer chain
	 */
	uint8_t request_type;

	char *current_position = package_buffer->payload;
	char *end_ptr = current_position + package_buffer->len;
	err_t error_rv;
	if(package_buffer->len > 4
			&& 0 == strncmp(current_position, "GET", 3)) {
		//GET REQUEST
		current_position += 4;
		request_type = GET_REQUEST;
	}
	else if(package_buffer->len > 5
			&& 0 == strncmp(current_position, "POST", 4)) {
		//POST REQUEST
		current_position += 5;
		request_type= POST_REQUEST;
	}
	else {
		//drop
		return ERR_OK;
	}
	char *location = current_position;

	while(current_position < end_ptr && *current_position != ' ') {
		++current_position;
	}
	// Zero terminate the location for the request handler.
		*current_position = '\0';
		++current_position;

		// Go to the end of the first line.
		while(current_position < end_ptr && strncmp(current_position - 1, "\r\n", 2) != 0) {
			++current_position;
		}

		if(current_position == end_ptr) {
			// We have an invalid request, drop it.
			return ERR_VAL;
		}
		++current_position;

		uint32_t length = 0;

		// Parse all headers.
		while(current_position < end_ptr && strncmp(current_position - 4, "\r\n\r\n", 4) != 0) {
			char *header_start = current_position;
			while(current_position < end_ptr && strncmp(current_position - 1, "\r\n", 2) != 0) {
				++current_position;
			}

			if(header_start + 16 < end_ptr && strncmp(header_start, "Content-Length: ", 16) == 0) {
				length = atoi(header_start + 16);
			}
			++current_position;
		}

		if(strncmp(current_position - 4, "\r\n\r\n", 4) != 0) {
			// We have an invalid request, drop it.
			return ERR_VAL;
		}

		uint32_t outlen = 0;
		bool free_result = false;
		char* response = server_request_handler(location, request_type, current_position, length, &outlen, &free_result);
		error_rv = altcp_write(
			package_control_block
			, response
			, outlen
			, 0
		);
		if(free_result) {
			free(response);
		}
		tcp_state->state = TCP_CLOSING;
		return error_rv;
}

static void tcp_cb_free(struct tcp_cb_state *tcp_state)
{
	if (tcp_state != NULL) {
		if (tcp_state->package_buffer != NULL)
			pbuf_free(tcp_state->package_buffer);
		mem_free(tcp_state);
	}
}

static void tcp_cb_close(struct altcp_pcb *package_control_block, struct tcp_cb_state *tcp_state)
{
	altcp_arg(package_control_block, NULL);
	altcp_sent(package_control_block, NULL);
	altcp_recv(package_control_block, NULL);
	altcp_err(package_control_block, NULL);
	altcp_poll(package_control_block, NULL,0);

	if (NULL != tcp_state)
		tcp_cb_free(tcp_state);
	if(NULL != package_control_block)
		altcp_close(package_control_block);
}

static void tcp_cb_send(struct altcp_pcb *package_control_block, struct tcp_cb_state *tcp_state)
{
	struct pbuf *current_package_buffer;
	err_t write_error = ERR_OK;

	while(
			   (write_error == ERR_OK)
			&& (tcp_state->package_buffer != NULL)
			&& (tcp_state->package_buffer->len <= altcp_sndbuf(package_control_block))

		 )
	{
		current_package_buffer = tcp_state->package_buffer;

		write_error = altcp_write(
				package_control_block
				, tcp_state->package_buffer->payload
				, tcp_state->package_buffer->len
				, 1
				);
		if(write_error == ERR_OK)
		{
			uint16_t package_length;
			package_length = tcp_state->package_buffer->len;

			current_package_buffer = tcp_state->package_buffer;
			tcp_state->package_buffer = tcp_state->package_buffer->next;

			//if there is a next buffer, create the new reference
			if(tcp_state->package_buffer != NULL)
				pbuf_ref(tcp_state->package_buffer);

			//clear the chain
			//
			pbuf_free(current_package_buffer);

			//read more data
			altcp_recved(package_control_block, package_length);
		}
		else if (write_error == ERR_MEM)
		{
			//low on memory, defer to key/ try later
			tcp_state->package_buffer = current_package_buffer;
    	}
    }
}

static err_t tcp_cb_sent(void *arg, struct altcp_pcb *package_control_block, uint16_t len)
{
    struct tcp_cb_state *tcp_state;
    
    LWIP_UNUSED_ARG(len);
    
    tcp_state = NULL;
    tcp_state = (struct tcp_cb_state*) arg;
    if(NULL != tcp_state) {
        tcp_state->retries = 0;
        if(tcp_state->package_buffer != NULL) {
            altcp_sent(package_control_block, tcp_cb_sent);
            tcp_cb_send(package_control_block, tcp_state);
        }
        else if(TCP_CLOSING == tcp_state->state) {
            tcp_cb_close(package_control_block, tcp_state);
        }
    }

}

static void tcp_cb_error(void *arg, err_t err)
{
    struct tcp_cb_state *tcp_state;

    LWIP_UNUSED_ARG(err);

    tcp_state = (struct tcp_cb_state *)arg;

    tcp_cb_free(tcp_state);
}

static err_t tcp_cb_poll(void *arg, struct altcp_pcb *package_control_block)
{
	err_t error_rv;
	struct tcp_cb_state *tcp_state;
	tcp_state = NULL;
	tcp_state = (struct tcp_cb_state*) arg;
	if(NULL != tcp_state)
	{
		if(NULL != tcp_state->package_buffer )
		{
			//another buffer in chain
			tcp_cb_send(package_control_block, tcp_state);
		}
		else if(TCP_CLOSING == tcp_state->state) {
				tcp_cb_close(package_control_block, tcp_state);
		}
	}
	else {
		altcp_abort(package_control_block);
		tcp_cb_close(package_control_block, tcp_state);
		error_rv = ERR_ABRT;
	}
	return error_rv;
}

static err_t tcp_cb_recv(void *arg, struct altcp_pcb *package_control_block, struct pbuf *package_buffer, err_t recv_error)
{
	struct tcp_cb_state *tcp_state;
    err_t error_rv;
    error_rv = ERR_OK;


    LWIP_ASSERT("arg != NULL",arg != NULL);
    tcp_state = (struct tcp_cb_state *)arg;

    if(ERR_OK == recv_error && NULL != package_buffer) {
    	error_rv = tcp_cb_parse_http(package_control_block, tcp_state, package_buffer);
    	if (ERR_OK == error_rv)
    		altcp_sent(package_control_block, tcp_cb_sent);
    }
    if (ERR_OK != recv_error || ERR_OK != error_rv) {
    	pbuf_free(package_buffer);
    	tcp_cb_close(package_control_block, tcp_state);
    	error_rv = ERR_VAL;
    }
    return error_rv;
}
static err_t tcp_cb_accept(void *arg, struct altcp_pcb *new_package_control_block, err_t accpet_error)
{
	err_t error_rv;
    error_rv = ERR_OK;
    struct tcp_cb_state *tcp_state;

    LWIP_UNUSED_ARG(arg);
    if ((ERR_OK != accpet_error) || (NULL == new_package_control_block)) {
        return ERR_VAL;
    }

    /* Unless this pcb should have NORMAL priority, set its priority now.
       When running out of pcbs, low priority pcbs can be aborted to create
       new pcbs of higher priority. */
    altcp_setprio(new_package_control_block, TCP_PRIO_MIN);

    tcp_state = NULL;
    tcp_state = (struct tcp_cb_state *)mem_malloc(sizeof(struct tcp_cb_state));
    if (NULL != tcp_state) {
        tcp_state->state = TCP_ACCEPTED;
        tcp_state->package_control_block = new_package_control_block;
        tcp_state->retries = 0;
        tcp_state->package_buffer = NULL;
        
        //init callbacks with tcp_state and new control block
        //arguments that shall be passed to callbacks
        // void *arg == tcp_state from here
        // altcp_pcb *package_buffer == new_package_control_block from here
        altcp_arg(new_package_control_block, tcp_state);
        //bind the callbacks to the altcp funcs
        altcp_recv(new_package_control_block, tcp_cb_recv);
        altcp_err(new_package_control_block, tcp_cb_error);
        altcp_poll(new_package_control_block, tcp_cb_poll, 0);
        altcp_sent(new_package_control_block, tcp_cb_sent);
        error_rv = ERR_OK;
    }
    else {
        error_rv = ERR_MEM;
    }
    return error_rv;
}

void tcp_cb_init( struct altcp_pcb *package_control_block)
{
	err_t error;
	error = ERR_OK;
	package_control_block = altcp_tcp_new_ip_type(IPADDR_TYPE_V4);
	if(NULL != package_control_block)
		error = altcp_bind(package_control_block, IP_ANY_TYPE, 80);
	switch(error) {
	default: {
		package_control_block = altcp_listen(package_control_block);
		altcp_accept(package_control_block, tcp_cb_accept);
	}break;
	}
}

