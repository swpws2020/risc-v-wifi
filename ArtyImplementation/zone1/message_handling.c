#include "message_handling.h"

static int msg_handler(const int zone, const char *msg) {

    // consumed messages are processed locally and not forwarded to the broker
    int consumed = 0;

    if (strcmp("ping", msg)==0){
        MZONE_SEND(zone, "pong");
        consumed = 1;

    } else if (strcmp("broker hello", msg)==0){
        MZONE_SEND(zone, "online");

    }

    return consumed;

}
